import java.util.Scanner;
public class Statistics {

	public static void main(String[] args)
{	
	double[] num = new double [5];
	//set up an array so I can get min and max with 5 entries being the maximum
	Scanner keyboard = new Scanner(System.in);
	//named scanner variable to keyboard for system input
	System.out.println("Enter first number");
	//prompt to enter 1 of 5 digits
	num[0] = keyboard.nextDouble();
	//scanner takes in next double entered by user
	System.out.println("Enter second number");
	num[1] = keyboard.nextDouble();
	System.out.println("Enter third number");
	num[2] = keyboard.nextDouble();
	System.out.println("Enter fourth number");
	num[3] = keyboard.nextDouble();
	System.out.println("Enter fifth number");
	num[4] = keyboard.nextDouble();
	System.out.printf("The five numbers you have entered are: %.2f, %.2f, %.2f, %.2f, %.2f ", num[0], num[1], num[2], num[3], num[4]);
	//displays all 5 numbers using .2f for floating decimal up to two places
	double sum;
	sum = num[0]+num[1]+num[2]+num[3]+num[4];
	double average;
	average = sum/5;
	//divided the average by max number of inputs
	System.out.println("The sum of those numbers is equal to " + sum);
	System.out.println("The average is " + average);
	
	}

}
